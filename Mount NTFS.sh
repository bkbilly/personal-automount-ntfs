#!/bin/bash
sudo fdisk -l | grep NTFS | cut -d ' ' -f1 > /tmp/filesystem

#Check Disks
while read line
do
	fstab=$(cat /etc/fstab | grep $line)
	if [ "$fstab" == "" ]
	then
		nexist="FALSE"
	else
		nexist="TRUE"
	fi
	name=$(sudo ntfslabel -f $line | sed 's/ /-/g')
	asdf="$asdf '$nexist' '$line' '$name'"
done </tmp/filesystem

eval zenity --list --checklist --title="Mount\ NTFS" --text="Select\ Partitions\ you\ want\ to\ Mount" --column="Add" --column="Device" --column="Name" $asdf > /tmp/selected
sed 's/|/\n/g' /tmp/selected > /tmp/temp
cp /tmp/temp /tmp/selected

#Delete entryies from fstab
sudo sed -i '/file\ system/ d' /etc/fstab
sudo sed -i '/ntfs/ d' /etc/fstab

#Edit fstab
if [ -s /tmp/selected ]
then
	while read line
	do
		name=$(sudo ntfslabel -f $line | sed 's/ /-/g')
		echo "# <file system>		<mount point>		<type>	<options>		<dump>	<pass>" | sudo tee -a /etc/fstab >> /dev/null
		echo "$line		/media/$name	ntfs	defaults		0	0" | sudo tee -a /etc/fstab >> /dev/null
	done </tmp/selected
else
	echo -e "\n\n----------------------\n   Nothing Selected\n----------------------\n"
fi

#Check for Problems
while read line
do
	fstab=$(cat /etc/fstab | grep $line)
	if [ "$fstab" == "" ]
	then
		zenity --warning --text="Something went wrong\nMake sure you run on Super User this script"
	else
		echo -e "\n----------------------\n    DONE $line\n----------------------"
	fi
done </tmp/selected

rm /tmp/selected /tmp/filesystem /tmp/temp
